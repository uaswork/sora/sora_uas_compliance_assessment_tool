<?php
    require_once ("crypt.php");
    session_start();

    header('Content-Type: application/json');

    $aResult = array();

    //Default checks if function called exists and the post request correctly contains functionname and arguments
    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

    if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

    if( !isset($aResult['error']) ) {

        //Select the right function and handle data with arguments
        switch($_POST['functionname']) {
            case 'encrypt':
                if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 2) ) {
                    $aResult['error'] = 'Error in arguments!';
                }
                else {
                    //encrypt and return encrypted value
                    $name_encrypted = encrypt($_POST['arguments'][0]);
                    $email_encrypted = encrypt($_POST['arguments'][1]);

                    //set array to result, gets json encoded later
                    $aResult['result'] = array('name' => $name_encrypted, 'email' => $email_encrypted);
                }
                break;

            case 'saveinfosession':
                if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 4) ) {
                    $aResult['error'] = 'Error in arguments!';
                }
                else {
                    //encrypt and return encrypted value
                    $info->name = $_POST['arguments'][0];
                    $info->email = $_POST['arguments'][1];
                    $info->uav = $_POST['arguments'][2];
                    $info->sail = $_POST['arguments'][3];

                    //save in session
                    $_SESSION['db_information'] = json_encode($info);
                    $aResult['result'] = "Success, saved in session";
                }
                break;

            default:
                $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
                break;
        }

    }

    echo json_encode($aResult);

?>