<?php include("dataHandler.php");?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>SORA V2.0 Questionnaire: Overview of integrity and assurance requirements and the connected questions.</title>
	</head>
<body>
	<?php
		$sail = $_GET["sail"];
		$curr_oso = $_GET["oso"];
		echo "<h1>SORA V2.0 Questionnaire Overview</h1>";
		while($test_data[$curr_oso]["next_step"][$sail - 2] != "end"){
			$int_low_nr = sizeof($test_data[$curr_oso]["integrity_low"]);
			$int_medium_nr = sizeof($test_data[$curr_oso]["integrity_medium"]);
			$int_high_nr = sizeof($test_data[$curr_oso]["integrity_high"]);
			$ass_low_nr = sizeof($test_data[$curr_oso]["assurance_low"]);
			$ass_medium_nr = sizeof($test_data[$curr_oso]["assurance_medium"]);
			$ass_high_nr = sizeof($test_data[$curr_oso]["assurance_high"]);
			$robustnessReq = $test_data[$curr_oso]["sail_reqs"][$sail - 2];

			echo "<h2>".$test_data[$curr_oso]["oso_nr"]."</h2>";
			echo "<p>".$test_data[$curr_oso]["description"]."</p>";
			echo "<h3>Low criteria and questions</h3>";
			echo "<h4>Integrity</h4>";
			for($x = 0; $x < $int_low_nr; $x++){
				#echo $x + 1,". ".$test_data[$curr_oso]["integrity_low"][$x]."<br>";
				echo $test_data[$curr_oso]["integrity_low"][$x]."<br>";
			}
			echo "<h4>Assurance</h4>";
			for($x = 0; $x < $ass_low_nr; $x++){
				echo $test_data[$curr_oso]["assurance_low"][$x]."<br>";
			}
			echo "<h4>Questions</h4>";
			for($x = 0; $x < sizeof($test_data[$curr_oso]["questions_low"]); $x++){
				echo '<div class="" data-toggle="tooltip" data-placement="right" data-html="true" title="<i>'.$test_data[$curr_oso]["questions_low"][$x][1].'</i>">'.$test_data[$curr_oso]["questions_low"][$x][0].'</div><br>';
			}
			if($robustnessReq > 0){
				echo "<h3>Medium criteria and questions</h3>";
				echo "<h4>Integrity</h4>";
				for($x = 0; $x < $int_medium_nr; $x++){
					echo $test_data[$curr_oso]["integrity_medium"][$x]."<br>";
				}
				echo "<h4>Assurance</h4>";
				for($x = 0; $x < $ass_medium_nr; $x++){
					echo $test_data[$curr_oso]["assurance_medium"][$x]."<br>";
				}
				echo "<h4>Questions</h4>";
				for($x = 0; $x < sizeof($test_data[$curr_oso]["questions_medium"]); $x++){
					echo '<div class="" data-toggle="tooltip" data-placement="right" data-html="true" title="<i>'.$test_data[$curr_oso]["questions_medium"][$x][1].'</i>">'.$test_data[$curr_oso]["questions_medium"][$x][0].'</div><br>';
				}
				if($robustnessReq > 1){
					echo "<h3>High criteria and questions</h3>";
					echo "<h4>Integrity</h4>";
					for($x = 0; $x < $int_high_nr; $x++){
						echo $test_data[$curr_oso]["integrity_high"][$x]."<br>";
					}
					echo "<h4>Assurance</h4>";
					for($x = 0; $x < $ass_high_nr; $x++){
						echo $test_data[$curr_oso]["assurance_high"][$x]."<br>";
					}
					echo "<h4>Questions</h4>";
					for($x = 0; $x < sizeof($test_data[$curr_oso]["questions_high"]); $x++){
						echo '<div class="" data-toggle="tooltip" data-placement="right" data-html="true" title="<i>'.$test_data[$curr_oso]["questions_high"][$x][1].'</i>">'.$test_data[$curr_oso]["questions_high"][$x][0].'</div><br>';
					}
				}
			}
			$curr_oso = $test_data[$curr_oso]["next_step"][$sail - 2];
		}
	?>
</body>
</html>
