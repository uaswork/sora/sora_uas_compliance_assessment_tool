<?php

/***
 * Class DatabaseHandler
 * Using PDO for easier life and better security. Great guide about it here: https://phpdelusions.net/pdo
 */
class DatabaseHandler {
	/**
	* PDO object
	* @var \PDO
	*/
	private $db;

	/**
	* Initialize the object with a specified PDO object
	* @param \PDO $pdo
	*/
	public function __construct(){
		try {
			// Create (connect to) SQLite database in file
			$this->db = new PDO('sqlite:private/sora.db');

			// Set errormode to exceptions
			$this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e) {
			// Print PDOException message
			echo $e->getMessage();
		}
	}

	/***
	* Function used to save data from a completed surveys
	* @param $name - Users name
	* @param $email - Users email
	* @param $uav - UAV the survey is about
	* @param $sail - Target SAIL level
	* @param $answers - All Answers
	*/
	function addAnswerData($name, $email, $uav, $sail, $answers) {
		try{
			// make sql statement
			$sql = 'INSERT INTO QUESTIONNAIRE(ID, NAME, EMAIL, UAV, SAIL, ANSWERS) ' . 'VALUES(:id, :name, :email, :uav, :sail, :answers)';
			//prepare sql. Gives security for sql injection - https://xkcd.com/327/
			$stmt = $this->db->prepare($sql);

			//execute sql with values given. ID should be overwritten by database
			$stmt->execute([
				'id' => null,
				'name' => $name,
				'email' => $email,
				'uav' => $uav,
				'sail' => $sail,
				'answers' => $answers]);
			}
			catch(PDOException $e) {
				// Print PDOException message
				echo $e->getMessage();
			}
		}

		/***
		* Function to output and show current data in table. Used for debugging
		*/
		function printTable(){
			try {
				// Select all data from db
				$result = $this->db->query('SELECT * FROM QUESTIONNAIRE');

				foreach ($result as $row) {
					echo "Id: " . $row['ID'] . "\n" . "<br/>";
					echo "Name: " . $row['NAME'] . "\n" . "<br/>";
					echo "Email: " . $row['EMAIL'] . "\n" . "<br/>";
					echo "UAV: " . $row['UAV'] . "\n" . "<br/>";
					echo "SAIL: " . $row['SAIL'] . "\n" . "<br/>";
					echo "Answers: " . $row['ANSWERS'] . "\n" . "<br/>";
					echo "\n"."<br/>";
				}
			} catch (PDOException $e) {
				// Print PDOException message
				echo $e->getMessage();
			}
		}
		/**
		* Function to create table if recreating the database
		*/
		function createTable() {
			try {
				// Create table messages
				$this->db->exec("CREATE TABLE QUESTIONNAIRE
					(ID INTEGER PRIMARY KEY AUTOINCREMENT,
						NAME TEXT NOT NULL,
						EMAIL TEXT NOT NULL,
						UAV TEXT NOT NULL,
						SAIL INT NOT NULL,
						ANSWERS TEXT NOT NULL)");
					} catch (PDOException $e) {
						// Print PDOException message
						echo $e->getMessage();
					}
				}
				/**
				* Function to delete table QUESTIONNAIRE. WARNING: Will loose all data in table
				*/
				function dropTable() {
					try {
						// Drop table QUESTIONNAIRE from file db
						$this->db->exec("DROP TABLE QUESTIONNAIRE");
					} catch (PDOException $e) {
						// Print PDOException message
						echo $e->getMessage();
					}
				}
				function closeDbConnection() {
					$this->db = null;
				}
			}
?>
