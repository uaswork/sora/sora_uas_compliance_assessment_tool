<?php

function getResponse($answer){
    require("dataHandler.php");
    //todo Logic.... Kristian has to do this logic... Will probably be massive if-sentences.
    // If answers to other questions is needed, this function can be extended to also carry a copy of the entire array
    // besides the single answer object. Then you can iterate through it to check other answers.

    //Data available in variable
    $answer->oso;                   //eg. OSO02
    $answer->question;              //eg. 0
    $answer->question_level;        //eg. 1         NOTE: is using the int. use decodeQuestionLevel() to change to text
    $answer->answer;                //eg. Y         NOTE: uses Y, N, U
    $answer->answer_specific;       //eg. nonIntrusive

    if($answer->answer == "N"){
      return $test_data[$answer->oso][decodeQuestionLevel($answer->question_level)][$answer->question][2];
    }
    else{
      return "Satisfying answer";
    }
}

function decodeQuestionLevel($lvl) {
    //decode integer levels to what they are called in data fields to obtain correct data for viewing
    switch($lvl) {
        case 0:
            return "questions_low";
        case 1:
            return "questions_medium";
        case 2:
            return "questions_high";

        default:
            return $lvl;
    }
}

function decodeAnswer($ans) {
    //decode short-name to full answer for viewing
    switch ($ans) {
        case "Y":
            return "Yes";
        case "U":
            return "Don't know";
        case "N":
            return "No";

        default:
            return $ans;
    }
}

function decodeSpecificAnswer($answer_specific){
    //decode from short-name to full name for viewing
    switch ($answer_specific) {
        case "manual":
            return "The UAS manual";

        case "uavWebpage":
            return "The UAS manufacturer's web page";

        case "otherWebpage":
            return "Other web page";

        case "nonIntrusive":
            return "Nonintrusive UAS inspection";

        case "Intrusive":
            return "Intrusive UAS inspection";

        case "other":
            //Is whatever they wrote as other, just return answer
            return $answer_specific;

        case "notFoundManual":
            return "The UAS manual does not contain this information";

        case "notFoundInternet":
            return "Could not find the information online";

        case "notFoundUasInspection":
            return "Could not tell from UAS inspection";

        default:
            return $answer_specific;
    }
}
?>
