<?php
	include("dataHandler.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- dev -->
		<link rel="stylesheet" href="dev/bootstrap.min.css">

		<!-- Bootstrap stylesheet -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<!-- Custom Stylesheet -->
		<link rel="stylesheet" href="styles.css?v=1">

		<title>SORA V2.0 UAS Assessment Tool: Welcome</title>
	</head>
<body>
<div class="container">
	<h1>SORA V2.0 UAS Assessment Tool</h1>
  <br/>
</div>
<div class="container col-lg-5">
	<div class="card mb-lg-4 box-shadow ">
		<div class="card-header alert alert-info">
			<p class="">Welcome! The purpose of this tool is to guide you through an assessment of your UAS, based on a desired Specific Assurance and Integrity Level (SAIL). Please choose a SAIL for which you wish to evaluate your UAS. This tool is a work in progress. Upon completion, you will be presented with a link to a short evaluation questionnaire. It would be greatly appreciated if you would take the time to fill out this evaluation, as it will help the future development of the tool.</p>
		</div>

		<div class="card-body row justify-content-center">
			<form class="" action = "" method = "post">
				<div class="form-group">
					<label for="input_name">Name</label>
					<input type="text" name="name" class="form-control" id="input_name" placeholder="Name">
				</div>
				<div class="form-group">
					<label for="input_email">Email address</label>
					<input type="email" name="email" class="form-control" id="input_email" aria-describedby="emailHelp" placeholder="Enter email">
				</div>
				<div class="form-group">
					<label for="input_dronetype">Example select</label>
					<select class="custom-select" id="input_dronetype" name="drone_type">
						<option value="DJIP4P" selected>DJI Phantom 4 Pro</option>
						<option value="DJII2">DJI Inspire 2</option>
						<option value="DJIM600">DJI Matrice 600 Pro</option>
						<!-- TODO: Other should be an input text field -->
						<option value="3">Other</option>
					</select>
				</div>
				<div class="form-group offset-lg-4">
					<div class="form-check custom-control custom-checkbox">
						<input class="custom-control-input" id="sail_lvl_2" type="radio" name="sail_lvl" required value = "2">
						<label class="custom-control-label" for="sail_lvl_2">SAIL II</label>
					</div>
					<div class="form-check custom-control custom-checkbox">
						<input class="custom-control-input" id="sail_lvl_3" type = "radio" name = "sail_lvl" required value = "3">
						<label class="custom-control-label" for="sail_lvl_3">SAIL III</label>
					</div>
					<div class="form-check custom-control custom-checkbox">
						<input class="custom-control-input" id="sail_lvl_4" type = "radio" name = "sail_lvl" required value = "4">
						<label class="custom-control-label" for="sail_lvl_4">SAIL IV</label>
					</div>
				</div>
				<div class="form-group offset-lg-4">
					<div class="form-check custom-control custom-checkbox">
						<input class="custom-control-input" id="consent" type="radio" name="consent" required value = "1">
						<label class="custom-control-label" for="consent">I accept the storage of my data for analysis.</label>
					</div>
				</div>
				<button role="button" id="submit_btn" class="btn btn-primary offset-lg-4">Start</button>
			</form>
		</div>
	</div>
	<!-- TODO: Place this button in a more discrete location. Very few users will need this. -->
</div>
<!-- Bootstrap dependencies -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- CUSTOM JAVASCRIPT -->
<script>
	//TODO: Note that this is one way of doing it. If you prefer to keep the link redirection in php, then it can
	//TODO: be changed to just emulate a form-post on every change of the radio-buttons.

	var base_link = "<?php echo $base_link ?>";
	var overview_link = "<?php echo $overview_link ?>";
	//ensure jquery is ready
	$(document).ready(function () {
		// Btn click handler
		$("#submit_btn").on('click', function (event){
			event.preventDefault();

            //save information in session
            var data = {};

            data.name = document.getElementById('input_name').value;
            data.email = document.getElementById('input_email').value;
            data.uav = document.getElementById('input_dronetype').value;
            data.sail = parseInt($('input[name=sail_lvl]:checked').val(), 10);

            saveInfoSession(data);
		});

		function saveCookieAndLoadNextSide() {
            //make empty array to save answers
            var array_answers = [];

            //get sail
            var sail = parseInt($('input[name=sail_lvl]:checked').val(), 10);

            //stringify cookies
            var json_array_string = JSON.stringify(array_answers);

            //save cookies
            document.cookie = "db_answers=" + json_array_string;

            //set first oso
            var oso = "na";
            switch (sail) {
                case 2:
                    oso = "OSO03";
                    break;
                case 3:
                    oso = "OSO02";
                    break;
                case 4:
                    oso = "OSO02";
                    break;
            }

            //Ensure a sail has been selected.
            if(oso === "na"){
                alert("Please select a sail value");
                return;
            }

            var check = "na";
            switch (consent) {
                case 1:
                    check = "checked";
                    break;
            }

            //Ensure consent has been given.
            //if(check === "na"){
            //    alert("Please consent to data storage for analysis");
            //    return;
            //}

            //go to link:
            var link = base_link + "&sail=" + sail + "&oso=" + oso + "&ques_lvl=0&ques=0";

            //redirect user to link
            window.location.href = link;
        }

		function saveInfoSession(data) {
            //Encrypt personal information though ajax call to php side
            jQuery.ajax({
                type: "POST",
                url: 'controller.php',
                dataType: 'json',
                data: {
                    functionname: 'saveinfosession',
                    arguments: [data.name, data.email, data.uav, data.sail]
                },
                success: function (data) {
                    //success, pass info to function and keep going
                    console.log(data);
                    saveCookieAndLoadNextSide();
                }
            });
        }

		//TODO: Can this be done smarter, or is it OK?
		$("#overview_btn").on('click', function (event){
			event.preventDefault();
			// get value from checked element and parse to int
			var sail = parseInt($('input[name=sail_lvl]:checked').val(), 10);

			//set first oso
			var oso = "na";
			switch (sail) {
				case 2:
					oso = "OSO03";
				break;
				case 3:
					oso = "OSO02";
				break;
				case 4:
					oso = "OSO02";
				break;
			}

			//ensure a sail has been selected. TODO probably doesn't matter as one sail is preselected
			if(oso === "na"){
				alert("Need to select a sail value");
				return;
			}

			//go to link:
			var link = overview_link + "&sail=" + sail + "&oso=" + oso + "&ques_lvl=0&ques=0";

			//redirect user to link
			window.location.href = link;
		});
	});
</script>
</body>
</html>
