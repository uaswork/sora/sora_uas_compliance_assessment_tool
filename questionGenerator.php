<?php
//require database handler
require_once("dataHandler.php");
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap stylesheet -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="styles.css?v=1">
    <title>SORA V2.0 UAS Assessment Tool: Questionnaire</title>
</head>
<body>
<?php
$sail = $_GET["sail"];
$curr_oso = $_GET["oso"];
$curr_ques_lvl = $_GET["ques_lvl"];
$curr_ques = $_GET["ques"];

$int_low_nr = sizeof($test_data[$curr_oso]["integrity_low"]);
$int_medium_nr = sizeof($test_data[$curr_oso]["integrity_medium"]);
$int_high_nr = sizeof($test_data[$curr_oso]["integrity_high"]);
$ass_low_nr = sizeof($test_data[$curr_oso]["assurance_low"]);
$ass_medium_nr = sizeof($test_data[$curr_oso]["assurance_medium"]);
$ass_high_nr = sizeof($test_data[$curr_oso]["assurance_high"]);

$q_warn = $curr_ques;

$robustnessReq = $test_data[$curr_oso]["sail_reqs"][$sail - 2];

#// DEBUG:
//echo $_COOKIE['db_answers'];
?>

<div class="container">
    <h1>SORA V2.0 UAS Assessment Tool</h1>
    <br/>
</div>

<div class="container col-lg-5">
    <div class="card mb-4">
        <div class="card-header alert alert-info">
            <?php echo "<h2 class='text-center'>".$test_data[$curr_oso]["title"]."</h2>"; ?>
            <!-- CHECK AND DISPLAY DESCRIPTION -->
            <div class="">
                <?php
                if($_GET["oso_descr"] == "true"){
                    echo "<h3>".$test_data[$curr_oso]["oso_nr"]."</h3>";
                    echo "<p>".$test_data[$curr_oso]["description"]."</p>";

                    echo "<h4>Integrity criteria:</h4>";
                    echo "<p>";
                    for($x = 0; $x < $int_low_nr; $x++){
                        #echo $x + 1,". ".$test_data[$curr_oso]["integrity_low"][$x]."<br>";
                        echo $test_data[$curr_oso]["integrity_low"][$x]."<br>";
                    }
                    if($robustnessReq > 0){
                        for($x = 0; $x < $int_medium_nr; $x++){
                            #echo $int_low_nr + $x + 1,". ".$test_data[$curr_oso]["integrity_medium"][$x]."<br>";
                            echo $test_data[$curr_oso]["integrity_medium"][$x]."<br>";
                        }

                        if($robustnessReq > 1){
                            for($x = 0; $x < $int_high_nr; $x++){
                                #echo $int_low_nr + $int_medium_nr + $x + 1,". ".$test_data[$curr_oso]["integrity_high"][$x]."<br>";
                                echo $test_data[$curr_oso]["integrity_high"][$x]."<br>";
                            }
                        }
                    }
                    echo "</p>";
                    echo "<h4>Assurance criteria:</h4>";
                    echo "<p>";
                    for($x = 0; $x < $ass_low_nr; $x++){
                        #echo $x + 1,". ".$test_data[$curr_oso]["assurance_low"][$x]."<br>";
                        echo $test_data[$curr_oso]["assurance_low"][$x]."<br>";
                    }
                    if($robustnessReq > 0){
                        for($x = 0; $x < $ass_medium_nr; $x++){
                            #echo $ass_low_nr + $x + 1,". ".$test_data[$curr_oso]["assurance_medium"][$x]."<br>";
                            echo $test_data[$curr_oso]["assurance_medium"][$x]."<br>";
                        }
                        if($robustnessReq > 1){
                            for($x = 0; $x < $ass_high_nr; $x++){
                                #echo $ass_low_nr + $ass_medium_nr + $x + 1,". ".$test_data[$curr_oso]["assurance_high"][$x]."<br>";
                                echo $test_data[$curr_oso]["assurance_high"][$x]."<br>";
                            }
                        }
                    }
                    echo "</p>";
                    echo "<br>";
                }
                ?>
            </div>
        </div>

        <div class="card-body justify-content-center">
            <form class="row justify-content-center" action = "" method = "post">
                <?php
                //todo debug
//                echo "curr_oso: " . $curr_oso . "<br/>";
//                echo "curr_ques_lvl: " . $curr_ques_lvl . "<br/>";
//                echo "robustnessReq: " . $robustnessReq. "<br/>";

                //TODO If the situration appears where we get redirected to a next oso, and that oso shouldn't show any questions due to sail-level,
                //then we will show blank. But if next oso always have at least one question it should work as expected.

                //change quest level to value in json data for matching
                if($curr_ques_lvl == 0){
                    $question_lvl = "questions_low";
                } elseif($curr_ques_lvl == 1){
                    $question_lvl = "questions_medium";
                } elseif($curr_ques_lvl == 2){
                    $question_lvl = "questions_high";
                }
                //echo out a question, as the current page should always show a question
                echo '<div class="" data-toggle="tooltip" data-placement="right" data-html="true" title="<i>'.$test_data[$curr_oso][$question_lvl][$curr_ques][1].'</i>">'.$test_data[$curr_oso][$question_lvl][$curr_ques][0].'</div>';

                //if current question is last question, increment to next level, otherwise increment to next question
                if($curr_ques < sizeof($test_data[$curr_oso][$question_lvl]) - 1){ //-1 as it is zero indexed
                    $curr_ques++;
                } else {
                    //next level
                    $curr_ques = 0;
                    $curr_ques_lvl++;

                    //check if new question level is lower or equal to robustnessReq
                    if($curr_ques_lvl > $robustnessReq) {
                        //go next oso instead
                        $curr_ques_lvl = 0;
                        $curr_oso = $test_data[$curr_oso]["next_step"][$sail - 2];
                    }
                }

                //if oso is end, go to end instead
                if($curr_oso == "end"){
                    $link = "result.php";
                } else{
                    $link = $base_link."&sail=".$sail."&oso=".$curr_oso."&ques_lvl=".$curr_ques_lvl."&ques=".$curr_ques;
                }

                //debug
//                echo "new curr_question: " . $curr_ques . "<br/>";
//                echo "new curr_quest_lvl: " . $curr_ques_lvl . "<br/>";
//                echo "new curr_oso: " . $curr_oso . "<br/>";
//                echo "question_level " . $question_lvl . "<br/>";
//

                ?>
            </form>
            <br/>
            <div class="row justify-content-center">
                <div class="custom-control-inline">
                    <button role="button" data-toggle="modal" data-target="#yesModal" id="" class="btn btn-success custom-control-inline">Yes</button>
                    <button role="button" data-toggle="modal" data-target="#unknownModal" id="" class="btn btn-warning custom-control-inline">Don't Know</button>
                    <button role="button" data-toggle="modal" data-target="#noModal" id="" class="btn btn-danger custom-control-inline">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="yesModal" tabindex="-1" role="dialog" aria-labelledby="yesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>From where do you know that this is correct?</p>
                <form class="" action = "" method = "post">
                    <div class="form-group">
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="option0" type="radio" name="source" required value = "manual">
                            <label class="custom-control-label" for="option0">The UAS manual</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="option1" type = "radio" name = "source" required value = "uavWebpage">
                            <label class="custom-control-label" for="option1">The UAS manufacturer's web page</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="option2" type = "radio" name = "source" required value = "otherWebpage">
                            <label class="custom-control-label" for="option2">Other web page</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="option3" type = "radio" name = "source" required value = "nonIntrusive">
                            <label class="custom-control-label" for="option3">Nonintrusive UAS inspection</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="option4" type = "radio" name = "source" required value = "Intrusive">
                            <label class="custom-control-label" for="option4">Intrusive UAS inspection</label>
                        </div>
                        <div class="form-check custom-control-inline custom-checkbox align-items-center">
                            <input class="custom-control-input" id="option_other" type="radio" name="source" required value="other">
                            &nbsp;<label class="custom-control-label" for="option_other">Other:</label>&nbsp;
                            <input type="text" class="form-control form-control-sm" name="other_source" placeholder="Describe other option">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-answer="Y" class="continue_btn btn btn-primary">Continue</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="unknownModal" tabindex="-1" role="dialog" aria-labelledby="unknownModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Why do you not know this?</p>
                <form class="" action = "" method = "post">
                    <div class="form-group">
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="unknown_option0" type="radio" name="source" required value = "notFoundManual">
                            <label class="custom-control-label" for="unknown_option0">The UAS manual does not contain this information</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="unknown_option1" type = "radio" name = "source" required value = "notFoundInternet">
                            <label class="custom-control-label" for="unknown_option1">Could not find the information online</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="unknown_option2" type = "radio" name = "source" required value = "notFoundUasInspection">
                            <label class="custom-control-label" for="unknown_option2">Could not tell from UAS inspection</label>
                        </div>
                        <div class="form-check custom-control-inline custom-checkbox align-items-center">
                            <input class="custom-control-input" id="unknown_option3" type="radio" name="source" required value="other">
                            &nbsp;<label class="custom-control-label" for="unknown_option3">Other:</label>&nbsp;
                            <input type="text" class="form-control form-control-sm" name="other_source" placeholder="Describe other option">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-answer="U" class="continue_btn btn btn-primary">Continue</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="noModal" tabindex="-1" role="dialog" aria-labelledby="noModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>From where do you know that this is not correct?</p>
                <form class="" action = "" method = "post">
                    <div class="form-group">
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="no_option0" type="radio" name="source" required value = "manual">
                            <label class="custom-control-label" for="no_option0">The UAS manual</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="no_option1" type = "radio" name = "source" required value = "uavWebpage">
                            <label class="custom-control-label" for="no_option1">The UAS manufacturer's web page</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="no_option2" type = "radio" name = "source" required value = "otherWebpage">
                            <label class="custom-control-label" for="no_option2">Other web page</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="no_option3" type = "radio" name = "source" required value = "nonIntrusive">
                            <label class="custom-control-label" for="no_option3">Nonintrusive UAS inspection</label>
                        </div>
                        <div class="form-check custom-control custom-checkbox">
                            <input class="custom-control-input" id="no_option4" type = "radio" name = "source" required value = "Intrusive">
                            <label class="custom-control-label" for="no_option4">Intrusive UAS inspection</label>
                        </div>
                        <div class="form-check custom-control-inline custom-checkbox align-items-center">
                            <input class="custom-control-input" id="no_option_other" type="radio" name="source" required value="other">
                            &nbsp;<label class="custom-control-label" for="no_option_other">Other:</label>&nbsp;
                            <input type="text" class="form-control form-control-sm"  id="testid" name="other_source" placeholder="Describe other option">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-answer="N" class="continue_btn btn btn-primary">Continue</button>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap dependencies -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<p id = "demo"></p>

<script>
    //enable tooltips.
    //TODO: add tooltips to elements like so : <div data-toggle="tooltip" data-placement="top" title="Tooltip on top">Tooltip on top</div>.
    //TODO: Described here: https://getbootstrap.com/docs/4.0/components/tooltips/
    $(function(){
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).ready(function(){
        $(".continue_btn").on('click', function(event){
            event.preventDefault();

            //get overall answer from data-answer on btn clicked.
            var answer = $(event.target).data("answer");

            //get specific answer
            var answer_specific = $(event.target).closest(".modal").find("input[name=source]:checked").val();

            //get other-text if other is selected
            if (answer_specific === "other") {
                answer_specific = $(event.target).closest(".modal").find("input[name=other_source]").val();

                //TODO if other is selected but is empty abort and alert user
                if (answer_specific === "") {
                    alert("Other can not be empty when selected!");
                    return;
                }
            }

            //Append answer to answer cookie.
            var answer_cookie = getCookie("db_answers");

            //decode as json
            var json_cookie = JSON.parse(answer_cookie);

            //create answer object
            var answer_string = {
                answer: answer,
                answer_specific: answer_specific,
                oso: getUrlParameter("oso"),
                question_level: getUrlParameter("ques_lvl"),
                question: getUrlParameter("ques"),
            };

            //update answers
            json_cookie.push(answer_string);

            //encode to json string
            var result = JSON.stringify(json_cookie);

            //save cookie
            document.cookie = "db_answers=" + result;

            ////Retrieve link for parsing.
            var link = "<?php echo $link ?>";
            //TODO: Add check on value select.
            window.location.href = link;
        });
    });

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

</script>
<!--Script for splitting and parsing the desired cookie-->
<script>
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if(c.indexOf(name) == 0)
                return c.substring(name.length,c.length);
        }
        return "";
    }
</script>
</body>
</html>
