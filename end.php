<?php
	require_once("databaseHandler.php");
	require_once("crypt.php");
	session_start();

	echo "You are done!<br/><br/>";


	//TODO decrypt personal information cookies

	$info = json_decode($_SESSION['db_information']);
	$answers = $_COOKIE['db_answers'];

	//TODO debug
    echo "Submitted results:"."<br/>";
	echo $info->name ."<br/>";
	echo $info->email."<br/>";
	echo $info->uav."<br/>";
	echo $info->sail."<br/>";
	echo $answers."<br/>";
	echo "<br/>";

	//encrypt name and email for GDPR
	$name_encrypted = encrypt($info->name);
	$email_encrypted = encrypt($info->email);

	echo "Encrypted Data <br/>";
	echo $name_encrypted . "<br/>";
	echo $email_encrypted . "<br/>";

	echo "<br/> Decrypted Data <br/>";
	echo decrypt($name_encrypted) . "<br/>";
	echo decrypt($email_encrypted) . "<br/>";

	//Make new database handler
	$database = new DatabaseHandler();

	// add answers
	$database->addAnswerData($name_encrypted, $email_encrypted, $info->uav, $info->sail, $answers);

	//TODO debugging: show current data in table
    echo "<br/><br/> Current data in database: <br/><br/>";
	$database->printTable();

	//close database
	$database->closeDbConnection();

?>